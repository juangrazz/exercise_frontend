import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign_up/sign_up.component';
import { HomeComponent } from './components/home/home.component';
import { AllUsersComponent } from './components/home/all-users/all-users.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'sign_up', component: SignUpComponent },
  { path: 'home', component: HomeComponent, canActivate: [AuthGuard]},
  { path: 'allusers', component: AllUsersComponent, canActivate: [AuthGuard]},
  { path: '**', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

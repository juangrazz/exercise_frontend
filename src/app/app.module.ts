import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign_up/sign_up.component';
import { CorrectModalComponent } from './components/modals/correct-modal/correct-modal.component';
import { FailModalComponent } from './components/modals/fail-modal/fail-modal.component';
import { HomeComponent } from './components/home/home.component';
import { AllUsersComponent } from './components/home/all-users/all-users.component';

import { AuthGuard } from "./auth.guard";
import { TokenInterceptorService } from './services/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SignUpComponent,
    CorrectModalComponent,
    FailModalComponent,
    HomeComponent,
    AllUsersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [AuthGuard, {provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }

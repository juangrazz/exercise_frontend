import { Component, OnInit } from '@angular/core';
import { User } from '../../../models/User';
import { UserService } from '../../../services/user.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.css']
})
export class AllUsersComponent implements OnInit {

  user: User;
  users: User[];
  searchByNameForm: FormGroup;

  constructor(private userService: UserService) { 
    this.user = JSON.parse(localStorage.getItem("user"));
  }

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers(){
    this.userService.getAllUsers().subscribe(
      res => {
        this.users = res;
        this.users.sort((a, b) => a.username.localeCompare(b.username));
      },
      err => {
        console.log(err);
      }
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { Message } from '../../models/Message';
import { UserService } from '../../services/user.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  user: User;
  updatedUser: User;

  updateForm: FormGroup;
  message: Message;

  constructor(private userService: UserService, private router: Router, private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem("user"));
    this.createForm();
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe(
      res => {
        this.message = res;
        localStorage.removeItem("user");
        $("#correctModalMessage").text(this.message.message);
        $('#correctModal').modal('show');
        $('#correctModal').on('hidden.bs.modal', function (e) {
          window.location.href = "/";
        });
      },
      err => console.log(err)
    );
  }

  updateUser(){
    this.fillUser();
    this.userService.updateUser(this.updatedUser).subscribe(
      res => {
        this.message = res;
        this.userService.getUserById(this.updatedUser._id).subscribe(
          res => {
            this.user = res;
            localStorage.setItem("user", JSON.stringify(this.user));
            $("#correctModalMessage").text(this.message.message);
            $('#correctModal').modal('show');
          },
          err => console.log(err)
        );
      },
      err => console.log(err)
    );
  }

  closeSession() {
    localStorage.removeItem("user");
    $("#correctModalMessage").text("Session closed correctly!");
    $('#correctModal').modal("show");
    $('#correctModal').on('hidden.bs.modal', function (e) {
      window.location.href = "/";
    });
  }

  fillUser() {
    this.updatedUser = {
      _id: this.user._id,
      password: null,
      username: this.updateForm.value.username,
      name: this.updateForm.value.name,
      lastname: this.updateForm.value.lastname,
      email: this.updateForm.value.email
    }
  }

  createForm() {
    this.updateForm = this.formBuilder.group({
      username: [this.user.username , Validators.required],
      name: [this.user.name, Validators.required],
      lastname: [this.user.lastname, Validators.required],
      email: [this.user.email, [Validators.required, Validators.pattern("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,5}")]],
    });
  }

  validateForm() {
    return this.updateForm.valid;
  }

  cleanForm() {
    this.updateForm.reset();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { LoginUser } from '../../models/login_user';
import { Router } from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loggedUser: LoginUser;

  constructor(private formBuilder: FormBuilder, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    this.createForm();
  }

  // FORM USERNAME GETTER
  get username() {
    return this.loginForm.get("username");
  }

  // FORM PASSWORD GETTER
  get password() {
    return this.loginForm.get("password");
  }

  // METHOD TO CREATE THE LOGIN FORM
  createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]],
    });
  }

  // METHOD TO VALIDATE THE FORM
  validateForm() {
    return this.loginForm.valid;
  }

  // METHOD TO FILL THE USER LOGGED
  fillUser() {
    this.loggedUser = {
      username: this.username.value,
      password: this.password.value
    }
  }

  // METHOD TO LOGIN
  login() {
    // THE FORM IS VALIDATED
    if (this.validateForm()) {

      // FILL THE USER
      this.fillUser();

      // GET DATABASE INFO ABOUT THE FORM USER
      this.userService.checkLoginUser(this.loggedUser.username, this.loggedUser.password).subscribe(
        res => {
          // CHECK IF THE USER OBJECT IS EMPTY
          if (res === null) {
            // IF IT IS EMPTY, I SHOW AN ERROR MODAL
            $("#failModalMessage").text("Incorrect entered details");
            $('#failModal').modal('show');
          } else {
            // I SAVE IN LOCALSTORAGE THE USER INFO AND REDIRECT TO /HOME
            localStorage.setItem("token", JSON.stringify(res.token));
            this.router.navigate(['/home']);
          }
        },
        err => {
          console.log(err);
        }
      );

    }
  }

}

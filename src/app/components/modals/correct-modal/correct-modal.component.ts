import { Component, OnInit } from '@angular/core';
import { SignUpComponent } from '../../sign_up/sign_up.component';

@Component({
  selector: 'app-correct-modal',
  templateUrl: './correct-modal.component.html',
  styleUrls: ['./correct-modal.component.css']
})
export class CorrectModalComponent implements OnInit {

  message: string;

  constructor() {
  }

  ngOnInit(): void {
  }

}

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { Message } from '../../models/Message';

declare var $: any;

@Component({
  selector: 'app-register',
  templateUrl: './sign_up.component.html',
  styleUrls: ['./sign_up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  signedUser: User;

  constructor(private formBuilder: FormBuilder, private userService: UserService) { }

  ngOnInit(): void {
    this.createForm();
  }

  createForm() {
    this.signUpForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required]],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.required, Validators.pattern("[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*@[a-zA-Z0-9_]+([.][a-zA-Z0-9_]+)*[.][a-zA-Z]{2,5}")]],
    });
  }

  validateForm() {
    return this.signUpForm.valid;
  }

  cleanForm() {
    this.signUpForm.reset();
  }

  fillUser() {
    this.signedUser = {
      username: this.signUpForm.value.username,
      password: this.signUpForm.value.password,
      name: this.signUpForm.value.name,
      lastname: this.signUpForm.value.lastname,
      email: this.signUpForm.value.email
    }
  }

  signUp() {
    if (this.validateForm()) {
      this.fillUser();

      this.userService.checkUser(this.signedUser.username).subscribe(
        res => {

          if (res.status === "false") {
            this.userService.createUser(this.signedUser).subscribe(
              res => {
                this.cleanForm();

                $("#correctModalMessage").text("User created!");
                $('#correctModal').modal('show')
              },
              err => console.log(err)
            );
          } else {
            $("#failModalMessage").text("User already exists");
            $('#failModal').modal('show');
          }

        },
        err => console.log(err)
      );
    } else {
      $("#failModalMessage").text("Incorrect entered details");
      $('#failModal').modal('show');
    }
  }
}

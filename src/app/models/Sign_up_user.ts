export interface SignUpUser {
    username: string,
    password: string,
    name: string,
    lastname: string,
    email: string,
    createdAt?: string,
    updatedAt?: string,
    _id?: string
}
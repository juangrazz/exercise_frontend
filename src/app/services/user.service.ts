import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from "@angular/common/http";
import { User } from '../models/User';
import { Message } from '../models/Message';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // API LOCATION
  URL_API = "http://localhost:4000/server";

  userGetted = new BehaviorSubject<User>({
    username: "",
    password: "",
    name: "",
    lastname: "",
    email: ""
  });

  constructor(private http: HttpClient) { }

  // METHOD TO CREATE AN USER
  createUser(user: User) {
    return this.http.post<Message>(`${this.URL_API}/user/createuser`, user);
  };

  // METHOD TO CHECK LOGIN USER
  checkLoginUser(username: string, password: string) {
    return this.http.get<any>(`${this.URL_API}/auth/checkuser/${username}&${password}`);
  }

  // METHOD TO GET ALL USERS
  getAllUsers() {
    return this.http.get<User[]>(`${this.URL_API}/user/getusers`);
  }

  // METHOD TO GET AN USER BY ID
  getUserById(id: string) {
    return this.http.get<User>(`${this.URL_API}/user/getuser/${id}`);
  }

  // METHOD TO CHECK IF USER EXISTS IN DB
  checkUser(username: string) {
    return this.http.get<Message>(`${this.URL_API}/user/checkUser/${username}`);
  }

  // METHOD TO UPDATE AN USER
  updateUser(user: User) {
    return this.http.put<Message>(`${this.URL_API}/user/updateuser/${user._id}`, user);
  }

  // METHOD TO DELETE AN USER
  deleteUser(id: string) {
    return this.http.delete<Message>(`${this.URL_API}/user/deleteuser/${id}`);
  }

  loggedIn() {
    if(localStorage.getItem("token")) {
      return true;
    } else {
      return false;
    }
  }
}
